#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "int_set.h"

#define NUM_TEST_KEYS 1024
#define NUM_TESTS 1024

void shuffle(int n, int s[n]) {
    for (int i = 0; i < n - 1; i++) {
        int swap_i = rand() % (n - i) + i;
        int tmp = s[i];
        s[i] = s[swap_i];
        s[swap_i] = tmp;
    }
}

int main() {
    srand(42);
    int test_keys[NUM_TEST_KEYS];
    for (int i = 0; i < NUM_TEST_KEYS; i++) {
        test_keys[i] = i;
    }
    for (int i = 0; i < NUM_TESTS; i++) {
        printf("running test %d\n", i);
        shuffle(NUM_TEST_KEYS, test_keys);
        int_set_t* t = int_set_make();
        for (int j = 0; j < NUM_TEST_KEYS; j++) {
            int_set_insert(t, test_keys[j]);
            int_set_check_invariants(t);
            assert(int_set_contains(t, test_keys[j]));
        }
        shuffle(NUM_TEST_KEYS, test_keys);
        for (int j = 0; j < NUM_TEST_KEYS; j++) {
            int_set_remove(t, test_keys[j]);
            int_set_check_invariants(t);
            assert(!int_set_contains(t, test_keys[j]));
        }
        int_set_free(t);
    }
    return 0;
}
