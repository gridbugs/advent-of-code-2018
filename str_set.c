#include "str_set.h"

#include <string.h>

#include "avl_tree_gen.h"

#define MAX_STRING 1024

int safe_strcmp(char* s1, char* s2) { return strncmp(s1, s2, MAX_STRING); }
AVL_TREE_SET_GEN(str, char*, safe_strcmp, free);

inline str_set_t* str_set_make() { return str_avl_tree_set_make(); }

inline void str_set_free(str_set_t* t) { str_avl_tree_set_free(t); }

inline bool str_set_contains(str_set_t* t, char* key) {
    return str_avl_tree_set_contains(t, key);
}

inline bool str_set_insert(str_set_t* t, char* key) {
    return str_avl_tree_set_insert(t, key);
}

inline bool str_set_remove(str_set_t* t, char* key) {
    return str_avl_tree_set_remove(t, key);
}

inline void str_set_check_invariants(str_set_t* t) {
    str_avl_tree_set_check_invariants(t);
}
