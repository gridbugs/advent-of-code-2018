#include <stdbool.h>
#include <stddef.h>

typedef struct vector vector_t;

vector_t* vector_make_empty(size_t element_size, bool data_are_pointers);
void vector_free(vector_t* t);

void vector_push(vector_t* t, void* element_ptr);
void vector_push_str(vector_t* t, char* s);

size_t vector_len(vector_t* t);

void* vector_data(vector_t* t);
