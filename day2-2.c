#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "avl_tree_gen.h"
#include "input.h"
#include "str_set.h"
#include "vector.h"

#define NUM_LETTERS 26
#define BUFSIZE 64
#define MAX_STRING 1024

vector_t* parse_input(FILE* input) {
    char buf[BUFSIZE];
    vector_t* vector = vector_make_empty(sizeof(char*), true);
    while (fgets(buf, BUFSIZE, input) != NULL) {
        // strip trailing newline
        buf[strnlen(buf, MAX_STRING) - 1] = '\0';
        vector_push_str(vector, buf);
    }
    return vector;
}

char* strdup_skip_ith_char(char* s, int i_to_skip) {
    char* buf = malloc(strnlen(s, MAX_STRING));
    int buf_i = 0;
    for (int s_i = 0; s[s_i] != '\0'; s_i++) {
        if (s_i != i_to_skip) {
            buf[buf_i] = s[s_i];
            buf_i++;
        }
    }
    buf[buf_i] = '\0';
    return buf;
}

char* solve(vector_t* input_vector) {
    size_t len = vector_len(input_vector);
    char** input_strings = vector_data(input_vector);
    size_t string_len = strnlen(input_strings[0], MAX_STRING);
    for (size_t index_to_ignore = 0; index_to_ignore < string_len;
         index_to_ignore++) {
        str_set_t* set = str_set_make();
        for (size_t i = 0; i < len; i++) {
            char* string_skipping_index =
                strdup_skip_ith_char(input_strings[i], index_to_ignore);
            if (!str_set_insert(set, string_skipping_index)) {
                char* ret = strdup(string_skipping_index);
                str_set_free(set);
                return ret;
            }
        }
        str_set_free(set);
    }
    assert(!"failed to solve");
}

int main(int argc, char* argv[]) {
    FILE* input = open_input(argc, argv);
    vector_t* input_vector = parse_input(input);
    char* common_chars = solve(input_vector);
    printf("%s\n", common_chars);
    vector_free(input_vector);
    return 0;
}
