#include <stdio.h>

#include "input.h"

#define BUFSIZE 64
char buf[BUFSIZE];

int main(int argc, char* argv[]) {
    FILE* input = open_input(argc, argv);
    int total = 0;
    while (fgets(buf, BUFSIZE, input) != NULL) {
        int x;
        sscanf(buf, "%d", &x);
        total += x;
    }
    printf("%d\n", total);
    return 0;
}
