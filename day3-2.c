#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "input.h"

#define MAX_REGIONS 2048
#define BUF_SIZE 1024
#define MATERIAL_SIDE 1000
#define MATERIAL_AREA (MATERIAL_SIDE * MATERIAL_SIDE)

typedef struct region {
    int x;
    int y;
    int w;
    int h;
    short id;
} region_t;

int parse_regions(FILE* input, region_t regions[]) {
    char buf[BUF_SIZE];
    int i = 0;
    while (fgets(buf, BUF_SIZE, input)) {
        assert(i < MAX_REGIONS);
        sscanf(buf, "#%hd @ %d,%d: %dx%d", &regions[i].id, &regions[i].x,
               &regions[i].y, &regions[i].w, &regions[i].h);
        i++;
    }
    return i;
}

bool add_region(short area_ids[MATERIAL_AREA], region_t region) {
    bool no_overlap = true;
    for (int i = 0; i < region.h; i++) {
        int y = i + region.y;
        for (int j = 0; j < region.w; j++) {
            int x = j + region.x;
            int index = y * MATERIAL_SIDE + x;
            if (area_ids[index] != region.id) {
                no_overlap = false;
            }
            area_ids[index] = region.id;
        }
    }
    return no_overlap;
}

int main(int argc, char* argv[]) {
    FILE* input = open_input(argc, argv);
    region_t regions[MAX_REGIONS];
    int num_regions = parse_regions(input, regions);
    short area_ids[MATERIAL_AREA] = {0};
    for (int i = 0; i < num_regions; i++) {
        add_region(area_ids, regions[i]);
    }
    int non_overlapping_id = 0;
    for (int i = 0; i < num_regions; i++) {
        if (add_region(area_ids, regions[i])) {
            non_overlapping_id = regions[i].id;
            break;
        }
    }
    assert(non_overlapping_id != 0);
    printf("%d\n", non_overlapping_id);
    return 0;
}
