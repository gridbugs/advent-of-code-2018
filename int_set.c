#include "int_set.h"

#include "avl_tree_gen.h"

static void ignore_free(int unused) { (void)unused; }
static int intcmp(int a, int b) { return a - b; }
AVL_TREE_SET_GEN(int, int, intcmp, ignore_free);

inline int_set_t* int_set_make() { return int_avl_tree_set_make(); }

inline void int_set_free(int_set_t* t) { int_avl_tree_set_free(t); }

inline bool int_set_contains(int_set_t* t, int key) {
    return int_avl_tree_set_contains(t, key);
}

inline bool int_set_insert(int_set_t* t, int key) {
    return int_avl_tree_set_insert(t, key);
}

inline bool int_set_remove(int_set_t* t, int key) {
    return int_avl_tree_set_remove(t, key);
}

inline void int_set_check_invariants(int_set_t* t) {
    int_avl_tree_set_check_invariants(t);
}
