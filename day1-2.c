#include <stdio.h>
#include <stdlib.h>

#include "input.h"
#include "int_set.h"
#include "vector.h"

int solve(vector_t* input_vector) {
    int total = 0;
    int_set_t* set = int_set_make();
    int_set_insert(set, 0);
    int* input_data = (int*)vector_data(input_vector);
    size_t input_len = vector_len(input_vector);
    while (true) {
        for (size_t i = 0; i < input_len; i++) {
            total += input_data[i];
            if (int_set_contains(set, total)) {
                return total;
            }
            int_set_insert(set, total);
        }
    }
}

vector_t* parse_input(FILE* input) {
#define BUFSIZE 64
    char buf[BUFSIZE];
    vector_t* vector = vector_make_empty(sizeof(int), false);
    while (fgets(buf, BUFSIZE, input) != NULL) {
        int x;
        sscanf(buf, "%d", &x);
        vector_push(vector, &x);
    }
    return vector;
}

int main(int argc, char* argv[]) {
    FILE* input = open_input(argc, argv);
    vector_t* input_vector = parse_input(input);
    int solution = solve(input_vector);
    printf("%d\n", solution);
    return 0;
}
