#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define AVL_TREE_MAP_GEN(PREFIX, key_t, data_t, cmp_fn, free_key_fn,           \
                         free_data_fn)                                         \
    typedef struct PREFIX##_node {                                             \
        key_t key;                                                             \
        data_t data;                                                           \
        struct PREFIX##_node* left;                                            \
        struct PREFIX##_node* right;                                           \
        struct PREFIX##_node* parent;                                          \
        short int balance;                                                     \
    } PREFIX##_node_t;                                                         \
                                                                               \
    typedef struct PREFIX##_avl_tree_map {                                     \
        PREFIX##_node_t* root;                                                 \
    } PREFIX##_avl_tree_map_t;                                                 \
                                                                               \
    PREFIX##_avl_tree_map_t* PREFIX##_avl_tree_map_make() {                    \
        PREFIX##_avl_tree_map_t* t = malloc(sizeof(PREFIX##_avl_tree_map_t));  \
        t->root = NULL;                                                        \
        return t;                                                              \
    }                                                                          \
                                                                               \
    static void PREFIX##_node_free(PREFIX##_node_t* node) {                    \
        if (node != NULL) {                                                    \
            PREFIX##_node_free(node->left);                                    \
            PREFIX##_node_free(node->right);                                   \
            free_key_fn(node->key);                                            \
            free_data_fn(node->data);                                          \
            free(node);                                                        \
        }                                                                      \
    }                                                                          \
                                                                               \
    void PREFIX##_avl_tree_map_free(PREFIX##_avl_tree_map_t* t) {              \
        PREFIX##_node_free(t->root);                                           \
        free(t);                                                               \
    }                                                                          \
                                                                               \
    static int PREFIX##_node_height(PREFIX##_node_t* node) {                   \
        if (node == NULL) {                                                    \
            return 0;                                                          \
        }                                                                      \
        int left_height = PREFIX##_node_height(node->left);                    \
        int right_height = PREFIX##_node_height(node->right);                  \
        return 1 + (left_height > right_height ? left_height : right_height);  \
    }                                                                          \
                                                                               \
    static void PREFIX##_node_check_invariants(PREFIX##_node_t* node) {        \
        if (node == NULL) {                                                    \
            return;                                                            \
        }                                                                      \
        assert(node->balance >= -1 && node->balance <= 1);                     \
        if (node->left != NULL) {                                              \
            assert(cmp_fn(node->left->key, node->key) < 0);                    \
        }                                                                      \
        if (node->right != NULL) {                                             \
            assert(cmp_fn(node->right->key, node->key) > 0);                   \
        }                                                                      \
        if (!(PREFIX##_node_height(node->right) -                              \
                  PREFIX##_node_height(node->left) ==                          \
              node->balance)) {                                                \
            assert(false);                                                     \
        }                                                                      \
        PREFIX##_node_check_invariants(node->left);                            \
        PREFIX##_node_check_invariants(node->right);                           \
    }                                                                          \
                                                                               \
    void PREFIX##_avl_tree_map_check_invariants(PREFIX##_avl_tree_map_t* t) {  \
        PREFIX##_node_check_invariants(t->root);                               \
    }                                                                          \
                                                                               \
    static PREFIX##_node_t* PREFIX##_node_find(PREFIX##_node_t* node,          \
                                               key_t key) {                    \
        if (node == NULL) {                                                    \
            return NULL;                                                       \
        }                                                                      \
        if (cmp_fn(key, node->key) < 0) {                                      \
            return PREFIX##_node_find(node->left, key);                        \
        }                                                                      \
        if (cmp_fn(key, node->key) > 0) {                                      \
            return PREFIX##_node_find(node->right, key);                       \
        }                                                                      \
        return node;                                                           \
    }                                                                          \
                                                                               \
    static bool PREFIX##_node_contains_key(PREFIX##_node_t* node, key_t key) { \
        return PREFIX##_node_find(node, key) != NULL;                          \
    }                                                                          \
                                                                               \
    bool PREFIX##_avl_tree_map_contains_key(PREFIX##_avl_tree_map_t* t,        \
                                            key_t key) {                       \
        return PREFIX##_node_contains_key(t->root, key);                       \
    }                                                                          \
    static PREFIX##_node_t* PREFIX##_rotate_left(PREFIX##_node_t* parent,      \
                                                 PREFIX##_node_t* child) {     \
        assert(child == parent->right);                                        \
        PREFIX##_node_t* mid = child->left;                                    \
        parent->right = mid;                                                   \
        if (mid != NULL) {                                                     \
            mid->parent = parent;                                              \
        }                                                                      \
        child->left = parent;                                                  \
        parent->parent = child;                                                \
        if (child->balance == 0) {                                             \
            child->balance = -1;                                               \
            parent->balance = +1;                                              \
        } else {                                                               \
            child->balance = 0;                                                \
            parent->balance = 0;                                               \
        }                                                                      \
        return child;                                                          \
    }                                                                          \
    static PREFIX##_node_t* PREFIX##_rotate_right(PREFIX##_node_t* parent,     \
                                                  PREFIX##_node_t* child) {    \
        assert(child == parent->left);                                         \
        PREFIX##_node_t* mid = child->right;                                   \
        parent->left = mid;                                                    \
        if (mid != NULL) {                                                     \
            mid->parent = parent;                                              \
        }                                                                      \
        child->right = parent;                                                 \
        parent->parent = child;                                                \
        if (child->balance == 0) {                                             \
            child->balance = +1;                                               \
            parent->balance = -1;                                              \
        } else {                                                               \
            child->balance = 0;                                                \
            parent->balance = 0;                                               \
        }                                                                      \
        return child;                                                          \
    }                                                                          \
    static PREFIX##_node_t* PREFIX##_rotate_left_right(                        \
        PREFIX##_node_t* parent, PREFIX##_node_t* child) {                     \
        assert(child == parent->left);                                         \
        assert(child->right != NULL);                                          \
        PREFIX##_node_t* inner_child = child->right;                           \
        PREFIX##_node_t* inner_child_left = inner_child->left;                 \
        child->right = inner_child_left;                                       \
        if (inner_child_left != NULL) {                                        \
            inner_child_left->parent = child;                                  \
        }                                                                      \
        inner_child->left = child;                                             \
        child->parent = inner_child;                                           \
        PREFIX##_node_t* inner_child_right = inner_child->right;               \
        parent->left = inner_child_right;                                      \
        if (inner_child_right != NULL) {                                       \
            inner_child_right->parent = parent;                                \
        }                                                                      \
        inner_child->right = parent;                                           \
        parent->parent = inner_child;                                          \
        if (inner_child->balance > 0) {                                        \
            parent->balance = 0;                                               \
            child->balance = -1;                                               \
        } else if (inner_child->balance < 0) {                                 \
            parent->balance = +1;                                              \
            child->balance = 0;                                                \
        } else {                                                               \
            parent->balance = 0;                                               \
            child->balance = 0;                                                \
        }                                                                      \
        inner_child->balance = 0;                                              \
        return inner_child;                                                    \
    }                                                                          \
    static PREFIX##_node_t* PREFIX##_rotate_right_left(                        \
        PREFIX##_node_t* parent, PREFIX##_node_t* child) {                     \
        assert(child == parent->right);                                        \
        assert(child->left != NULL);                                           \
        PREFIX##_node_t* inner_child = child->left;                            \
        PREFIX##_node_t* inner_child_right = inner_child->right;               \
        child->left = inner_child_right;                                       \
        if (inner_child_right != NULL) {                                       \
            inner_child_right->parent = child;                                 \
        }                                                                      \
        inner_child->right = child;                                            \
        child->parent = inner_child;                                           \
        PREFIX##_node_t* inner_child_left = inner_child->left;                 \
        parent->right = inner_child_left;                                      \
        if (inner_child_left != NULL) {                                        \
            inner_child_left->parent = parent;                                 \
        }                                                                      \
        inner_child->left = parent;                                            \
        parent->parent = inner_child;                                          \
        if (inner_child->balance > 0) {                                        \
            parent->balance = -1;                                              \
            child->balance = 0;                                                \
        } else if (inner_child->balance < 0) {                                 \
            parent->balance = 0;                                               \
            child->balance = +1;                                               \
        } else {                                                               \
            parent->balance = 0;                                               \
            child->balance = 0;                                                \
        }                                                                      \
        inner_child->balance = 0;                                              \
        return inner_child;                                                    \
    }                                                                          \
                                                                               \
    bool PREFIX##_avl_tree_map_insert(PREFIX##_avl_tree_map_t* t, key_t key,   \
                                      data_t data, data_t* existing) {         \
        PREFIX##_node_t* parent = t->root;                                     \
        PREFIX##_node_t* new_node = NULL;                                      \
        if (parent != NULL) {                                                  \
            while (true) {                                                     \
                if (cmp_fn(key, parent->key) < 0) {                            \
                    if (parent->left == NULL) {                                \
                        new_node = malloc(sizeof(PREFIX##_node_t));            \
                        parent->left = new_node;                               \
                        break;                                                 \
                    } else {                                                   \
                        parent = parent->left;                                 \
                    }                                                          \
                } else if (cmp_fn(key, parent->key) > 0) {                     \
                    if (parent->right == NULL) {                               \
                        new_node = malloc(sizeof(PREFIX##_node_t));            \
                        parent->right = new_node;                              \
                        break;                                                 \
                    } else {                                                   \
                        parent = parent->right;                                \
                    }                                                          \
                } else {                                                       \
                    if (existing != NULL) {                                    \
                        *existing = parent->data;                              \
                        parent->data = data;                                   \
                    }                                                          \
                    return false;                                              \
                }                                                              \
            }                                                                  \
        } else {                                                               \
            new_node = malloc(sizeof(PREFIX##_node_t));                        \
            t->root = new_node;                                                \
        }                                                                      \
        new_node->parent = parent;                                             \
        new_node->left = NULL;                                                 \
        new_node->right = NULL;                                                \
        new_node->key = key;                                                   \
        new_node->balance = 0;                                                 \
        new_node->data = data;                                                 \
        PREFIX##_node_t* child = new_node;                                     \
        while (parent != NULL) {                                               \
            PREFIX##_node_t* rotated_subtree = NULL;                           \
            PREFIX##_node_t* rotated_subtree_parent = NULL;                    \
            if (child == parent->left) {                                       \
                if (parent->balance < 0) {                                     \
                    rotated_subtree_parent = parent->parent;                   \
                    if (child->balance > 0) {                                  \
                        rotated_subtree =                                      \
                            PREFIX##_rotate_left_right(parent, child);         \
                    } else {                                                   \
                        rotated_subtree =                                      \
                            PREFIX##_rotate_right(parent, child);              \
                    }                                                          \
                } else {                                                       \
                    if (parent->balance > 0) {                                 \
                        parent->balance = 0;                                   \
                        break;                                                 \
                    }                                                          \
                    parent->balance = -1;                                      \
                    child = parent;                                            \
                    parent = child->parent;                                    \
                    continue;                                                  \
                }                                                              \
            } else {                                                           \
                assert(child == parent->right);                                \
                if (parent->balance > 0) {                                     \
                    rotated_subtree_parent = parent->parent;                   \
                    if (child->balance < 0) {                                  \
                        rotated_subtree =                                      \
                            PREFIX##_rotate_right_left(parent, child);         \
                    } else {                                                   \
                        rotated_subtree = PREFIX##_rotate_left(parent, child); \
                    }                                                          \
                } else {                                                       \
                    if (parent->balance < 0) {                                 \
                        parent->balance = 0;                                   \
                        break;                                                 \
                    }                                                          \
                    parent->balance = +1;                                      \
                    child = parent;                                            \
                    parent = child->parent;                                    \
                    continue;                                                  \
                }                                                              \
            }                                                                  \
            rotated_subtree->parent = rotated_subtree_parent;                  \
            if (rotated_subtree_parent == NULL) {                              \
                t->root = rotated_subtree;                                     \
            } else {                                                           \
                if (rotated_subtree_parent->left == parent) {                  \
                    rotated_subtree_parent->left = rotated_subtree;            \
                } else {                                                       \
                    assert(rotated_subtree_parent->right == parent);           \
                    rotated_subtree_parent->right = rotated_subtree;           \
                }                                                              \
            }                                                                  \
            break;                                                             \
        }                                                                      \
        return true;                                                           \
    }                                                                          \
                                                                               \
    PREFIX##_node_t* PREFIX##_node_min(PREFIX##_node_t* node) {                \
        while (node->left != NULL) {                                           \
            node = node->left;                                                 \
        }                                                                      \
        return node;                                                           \
    }                                                                          \
                                                                               \
    bool PREFIX##_avl_tree_map_remove(PREFIX##_avl_tree_map_t* t, key_t key,   \
                                      data_t* data) {                          \
        PREFIX##_node_t* node = PREFIX##_node_find(t->root, key);              \
        if (node == NULL) {                                                    \
            return false;                                                      \
        }                                                                      \
        if (data == NULL) {                                                    \
            free_data_fn(node->data);                                          \
        } else {                                                               \
            *data = node->data;                                                \
        }                                                                      \
        free_key_fn(node->key);                                                \
        bool shortened_left_child;                                             \
        PREFIX##_node_t* shortened_parent;                                     \
        PREFIX##_node_t* replacement;                                          \
        if (node->left == NULL) {                                              \
            if (node->right == NULL) {                                         \
                if (node->parent == NULL) {                                    \
                    t->root = NULL;                                            \
                    free(node);                                                \
                    return true;                                               \
                }                                                              \
                replacement = NULL;                                            \
                shortened_parent = node->parent;                               \
                shortened_left_child = node == node->parent->left;             \
            } else {                                                           \
                replacement = node->right;                                     \
                replacement->parent = node->parent;                            \
                replacement->balance = node->balance;                          \
                shortened_parent = replacement;                                \
                shortened_left_child = false;                                  \
            }                                                                  \
        } else {                                                               \
            if (node->right == NULL) {                                         \
                replacement = node->left;                                      \
                replacement->parent = node->parent;                            \
                replacement->balance = node->balance;                          \
                shortened_parent = replacement;                                \
                shortened_left_child = true;                                   \
            } else {                                                           \
                if (node->right->left == NULL) {                               \
                    replacement = node->right;                                 \
                    replacement->parent = node->parent;                        \
                    replacement->left = node->left;                            \
                    if (node->left != NULL) {                                  \
                        node->left->parent = replacement;                      \
                    }                                                          \
                    replacement->balance = node->balance;                      \
                    shortened_left_child = false;                              \
                    shortened_parent = replacement;                            \
                } else {                                                       \
                    replacement = PREFIX##_node_min(node->right);              \
                    replacement->parent->left = replacement->right;            \
                    if (replacement->right != NULL) {                          \
                        replacement->right->parent = replacement->parent;      \
                    }                                                          \
                    shortened_parent = replacement->parent;                    \
                    shortened_left_child = true;                               \
                    replacement->parent = node->parent;                        \
                    replacement->left = node->left;                            \
                    if (replacement->left != NULL) {                           \
                        replacement->left->parent = replacement;               \
                    }                                                          \
                    replacement->right = node->right;                          \
                    if (replacement->right != NULL) {                          \
                        replacement->right->parent = replacement;              \
                    }                                                          \
                    replacement->balance = node->balance;                      \
                }                                                              \
            }                                                                  \
        }                                                                      \
        PREFIX##_node_t* parent = node->parent;                                \
        if (parent == NULL) {                                                  \
            t->root = replacement;                                             \
        } else {                                                               \
            if (parent->left == node) {                                        \
                parent->left = replacement;                                    \
            } else {                                                           \
                assert(parent->right == node);                                 \
                parent->right = replacement;                                   \
            }                                                                  \
        }                                                                      \
        free(node);                                                            \
        if (shortened_parent == NULL) {                                        \
            return true;                                                       \
        }                                                                      \
        while (true) {                                                         \
            PREFIX##_node_t* shortened_grandparent = shortened_parent->parent; \
            PREFIX##_node_t* rotated_subtree = NULL;                           \
            short sibling_balance;                                             \
            if (shortened_left_child) {                                        \
                if (shortened_parent->balance > 0) {                           \
                    sibling_balance = shortened_parent->right->balance;        \
                    if (sibling_balance < 0) {                                 \
                        rotated_subtree = PREFIX##_rotate_right_left(          \
                            shortened_parent, shortened_parent->right);        \
                    } else {                                                   \
                        rotated_subtree = PREFIX##_rotate_left(                \
                            shortened_parent, shortened_parent->right);        \
                    }                                                          \
                } else {                                                       \
                    if (shortened_parent->balance == 0) {                      \
                        shortened_parent->balance = +1;                        \
                        break;                                                 \
                    }                                                          \
                    shortened_parent->balance = 0;                             \
                    if (shortened_grandparent == NULL) {                       \
                        break;                                                 \
                    }                                                          \
                    shortened_left_child =                                     \
                        shortened_grandparent->left == shortened_parent;       \
                    shortened_parent = shortened_grandparent;                  \
                    continue;                                                  \
                }                                                              \
            } else {                                                           \
                if (shortened_parent->balance < 0) {                           \
                    sibling_balance = shortened_parent->left->balance;         \
                    if (sibling_balance > 0) {                                 \
                        rotated_subtree = PREFIX##_rotate_left_right(          \
                            shortened_parent, shortened_parent->left);         \
                    } else {                                                   \
                        rotated_subtree = PREFIX##_rotate_right(               \
                            shortened_parent, shortened_parent->left);         \
                    }                                                          \
                } else {                                                       \
                    if (shortened_parent->balance == 0) {                      \
                        shortened_parent->balance = -1;                        \
                        break;                                                 \
                    }                                                          \
                    shortened_parent->balance = 0;                             \
                    if (shortened_grandparent == NULL) {                       \
                        break;                                                 \
                    }                                                          \
                    shortened_left_child =                                     \
                        shortened_grandparent->left == shortened_parent;       \
                    shortened_parent = shortened_grandparent;                  \
                    continue;                                                  \
                }                                                              \
            }                                                                  \
            rotated_subtree->parent = shortened_grandparent;                   \
            if (shortened_grandparent == NULL) {                               \
                t->root = rotated_subtree;                                     \
                break;                                                         \
            } else {                                                           \
                if (shortened_grandparent->left == shortened_parent) {         \
                    shortened_grandparent->left = rotated_subtree;             \
                    shortened_left_child = true;                               \
                } else {                                                       \
                    assert(shortened_grandparent->right == shortened_parent);  \
                    shortened_grandparent->right = rotated_subtree;            \
                    shortened_left_child = false;                              \
                }                                                              \
                if (sibling_balance == 0) {                                    \
                    break;                                                     \
                }                                                              \
                shortened_parent = shortened_grandparent;                      \
            }                                                                  \
        }                                                                      \
        return true;                                                           \
    }

#define AVL_TREE_SET_GEN(PREFIX, key_t, cmp_fn, free_key_fn)               \
    void __##PREFIX##_ignore_free_data(short data) { (void)data; }         \
    AVL_TREE_MAP_GEN(__##PREFIX, key_t, short, cmp_fn, free_key_fn,        \
                     __##PREFIX##_ignore_free_data);                       \
    typedef struct PREFIX##_avl_tree_set {                                 \
        __##PREFIX##_avl_tree_map_t map;                                   \
    } PREFIX##_avl_tree_set_t;                                             \
                                                                           \
    PREFIX##_avl_tree_set_t* PREFIX##_avl_tree_set_make() {                \
        PREFIX##_avl_tree_set_t* set =                                     \
            malloc(sizeof(PREFIX##_avl_tree_set_t));                       \
        set->map.root = NULL;                                              \
        return set;                                                        \
    }                                                                      \
                                                                           \
    void PREFIX##_avl_tree_set_free(PREFIX##_avl_tree_set_t* t) {          \
        __##PREFIX##_node_free(t->map.root);                               \
        free(t);                                                           \
    }                                                                      \
                                                                           \
    inline bool PREFIX##_avl_tree_set_insert(PREFIX##_avl_tree_set_t* t,   \
                                             key_t key) {                  \
        return __##PREFIX##_avl_tree_map_insert(&t->map, key, 0, NULL);    \
    }                                                                      \
                                                                           \
    inline bool PREFIX##_avl_tree_set_remove(PREFIX##_avl_tree_set_t* t,   \
                                             key_t key) {                  \
        return __##PREFIX##_avl_tree_map_remove(&t->map, key, NULL);       \
    }                                                                      \
                                                                           \
    inline bool PREFIX##_avl_tree_set_contains(PREFIX##_avl_tree_set_t* t, \
                                               key_t key) {                \
        return __##PREFIX##_avl_tree_map_contains_key(&t->map, key);       \
    }                                                                      \
                                                                           \
    inline void PREFIX##_avl_tree_set_check_invariants(                    \
        PREFIX##_avl_tree_set_t* t) {                                      \
        __##PREFIX##_avl_tree_map_check_invariants(&t->map);               \
    }
