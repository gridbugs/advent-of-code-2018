#include <assert.h>
#include <stdio.h>

#include "input.h"

#define MAX_REGIONS 2048
#define BUF_SIZE 1024
#define MATERIAL_SIDE 1000
#define MATERIAL_AREA (MATERIAL_SIDE * MATERIAL_SIDE)

typedef struct region {
    int x;
    int y;
    int w;
    int h;
    int id;
} region_t;

int parse_regions(FILE* input, region_t regions[]) {
    char buf[BUF_SIZE];
    int i = 0;
    while (fgets(buf, BUF_SIZE, input)) {
        assert(i < MAX_REGIONS);
        sscanf(buf, "#%d @ %d,%d: %dx%d", &regions[i].id, &regions[i].x,
               &regions[i].y, &regions[i].w, &regions[i].h);
        i++;
    }
    return i;
}

int add_region(short area_counts[MATERIAL_AREA], region_t region) {
    int count = 0;
    for (int i = 0; i < region.h; i++) {
        int y = i + region.y;
        for (int j = 0; j < region.w; j++) {
            int x = j + region.x;
            int index = y * MATERIAL_SIDE + x;
            if (area_counts[index] == 0) {
                area_counts[index] = 1;
            } else if (area_counts[index] == 1) {
                area_counts[index] = 2;
                count++;
            }
        }
    }
    return count;
}

int main(int argc, char* argv[]) {
    FILE* input = open_input(argc, argv);
    region_t regions[MAX_REGIONS];
    int num_regions = parse_regions(input, regions);
    short area_counts[MATERIAL_AREA] = {0};
    int total = 0;
    for (int i = 0; i < num_regions; i++) {
        total += add_region(area_counts, regions[i]);
    }
    printf("%d\n", total);
    return 0;
}
